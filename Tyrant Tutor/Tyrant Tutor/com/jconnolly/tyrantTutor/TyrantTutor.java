package com.jconnolly.tyrantTutor;

import java.util.Random ;

public class TyrantTutor {
	
	private String[] msgList ;
    private int usedCount ;
    private int listCapacity ;
    public int startTime ;
    public String modCode ;
    private Random rand ;
    
    public static void main(String[] args)
    {
    	TyrantTutor tt = new TyrantTutor(20, "CS4047", 3600) ;
    	tt.showList();
    	tt.moduleOf("Hello World") ;
    }
    
    
    public TyrantTutor()
    {
        modCode = "CS4411" ;
        DataGenerator gen = new DataGenerator(modCode, startTime) ;
        listCapacity = 20 ;
        msgList = new String[listCapacity] ;
        
        for(int i = 0 ; i < listCapacity ; i++)
        {
            msgList[i] = gen.getNext() ;
            usedCount++ ;
        }
        
    }
    
    public TyrantTutor(int listCapacity, String module, int classTime)
    {
        DataGenerator gen = new DataGenerator(module, startTime) ;
        msgList = new String[listCapacity] ;
        modCode = new String(module) ;
        startTime = classTime ;
        
        for(int i = 0 ; i < listCapacity ; i++)
        {
            msgList[i] = gen.getNext() ;
            usedCount++ ;
        }
    }
    
    public void insert(String attendMsg)
    {
        if(usedCount < msgList.length)
        {
            msgList[usedCount] = attendMsg ;
            usedCount++ ;
        } else {
            System.out.println("List is full!") ;
        }
    }
    
    public void showList()
    {
        for(int i = 0 ; i < msgList.length ; i++)
        {
            System.out.println(msgList[i]) ;
        }
    }
    
    public void weLoveInteraction(String msgText)
    {
        for(int i = 0 ; i < msgList.length ; i++)
        {
            System.out.println(phoneOf(msgList[i]) + " " + msgText) ;
        }
    }
    
    public String chosenOne()
    {
        rand = new Random() ;
        if(usedCount > 0)
        {
            int r = rand.nextInt(usedCount) ;
            return idOf(msgList[r]) + "  " + phoneOf(msgList[r]) ;
        } else {
            return "" ;
        }
    }
    
    public boolean whereIs(String ID)
    {
        int i ;
        for(i = 0 ; i < msgList.length && ID.compareTo(idOf(msgList[i])) != 0 ; i++) ;
        if(i < usedCount)
        {
            return true ;
        } else {
            return false ;
        }
    }
    
    public void punctuality(boolean late)
    {
        String[] lateAttend = new String[listCapacity] ;
        String[] onTime = new String[listCapacity] ;
        int i ;
        for(i = 0 ; i < msgList.length ; i++)
        {
            if(late == true && timeOf(msgList[i]) > startTime)
            {
                lateAttend[i] = idOf(msgList[i]) + " " + phoneOf(msgList[i]) ;
                System.out.println(lateAttend[i] + " " + "late") ;
            } else if(late == false && timeOf(msgList[i]) <= startTime) {
                    onTime[i] = idOf(msgList[i]) + " " + phoneOf(msgList[i]) ;
                    System.out.println(onTime[i] + " " + "on time") ;
            }
        }
    }
    
    public void questionTime()
    {
        boolean[] askedStudent = new boolean[usedCount] ;
        int r ;
        int i ;
        int notAskedYet ;
        for(i = 0 ; i < usedCount ; i++)
        {
            askedStudent[i] = false ;
        }
        notAskedYet = usedCount ;
        while(notAskedYet > 0)
        {
            r = rand.nextInt(usedCount) ;
            if(askedStudent[r] == false)
            {
                System.out.println(idOf(msgList[r])) ;
                askedStudent[r] = true ;
                notAskedYet-- ;
            }
        }
    }
        
    private String moduleOf(String msg)
    {
        String[] msgComponents = msg.split(" ");
        return msgComponents[0] ;
    }    

    private String idOf(String msg)
    {
        String[] msgComponents = msg.split(" ");
        return msgComponents[1] ;
    }    
        
    private String phoneOf(String msg)
    {
        String[] msgComponents = msg.split(" ");
        return msgComponents[2] ;
    } 
       
    private long  timeOf(String msg)
    {
        String[] msgComponents = msg.split(" ");
        return Integer.valueOf(msgComponents[3]).intValue() ;
    }

}
